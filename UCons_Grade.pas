unit UCons_Grade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  TFrm_Cons_Grade = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    StatusBar1: TStatusBar;
    Q_Cons_Grade: TOracleDataSet;
    Ds_Cons_Grade: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_GradeROWNUM: TFloatField;
    Q_Cons_GradeNUMERACAO1: TStringField;
    Q_Cons_GradeNUMERACAO2: TStringField;
    Q_Cons_GradeNUMERACAO3: TStringField;
    Q_Cons_GradeNUMERACAO4: TStringField;
    Q_Cons_GradeNUMERACAO5: TStringField;
    Q_Cons_GradeNUMERACAO6: TStringField;
    Q_Cons_GradeNUMERACAO7: TStringField;
    Q_Cons_GradeNUMERACAO8: TStringField;
    Q_Cons_GradeNUMERACAO9: TStringField;
    Q_Cons_GradeNUMERACAO10: TStringField;
    Q_Cons_GradeNUMERACAO11: TStringField;
    Q_Cons_GradeNUMERACAO12: TStringField;
    Q_Cons_GradeNUMERACAO13: TStringField;
    Q_Cons_GradeNUMERACAO14: TStringField;
    Q_Cons_GradeNUMERACAO15: TStringField;
    Q_Cons_GradeNUMERACAO16: TStringField;
    Q_Cons_GradeNUMERACAO17: TStringField;
    Q_Cons_GradeNUMERACAO18: TStringField;
    Q_Cons_GradeNUMERACAO19: TStringField;
    Q_Cons_GradeNUMERACAO20: TStringField;
    Q_Cons_GradeNUMERACAO21: TStringField;
    Q_Cons_GradeNUMERACAO22: TStringField;
    Q_Cons_GradeNUMERACAO23: TStringField;
    Q_Cons_GradeNUMERACAO24: TStringField;
    Q_Cons_GradeNUMERACAO25: TStringField;
    cxGrid_ConsultaDBTableView1ID_GRADE: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO1: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO2: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO3: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO4: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO5: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO6: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO7: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO8: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO9: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO10: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO11: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO12: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO13: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO14: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO15: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO16: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO17: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO18: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO19: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO20: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO21: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO22: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO23: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO24: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NUMERACAO25: TcxGridDBColumn;
    Q_Cons_GradeID_GRADE: TFloatField;
    Q_Cons_GradeVIGENCIA: TDateTimeField;
    cxGrid_ConsultaDBTableView1Vigencia: TcxGridDBColumn;
    Edt_Consulta: TEdit;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edt_ConsultaChange(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Grade: TFrm_Cons_Grade;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Grade.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_GRADE.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Grade.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Grade.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Grade.Edt_ConsultaChange(Sender: TObject);
begin
 with Q_Cons_Grade do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_grade',Edt_Consulta.Text);
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Grade.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Grade.Session := P_Sessao;
  Q_Cons_Grade.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

end.
