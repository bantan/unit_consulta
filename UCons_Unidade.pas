unit UCons_Unidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  TFrm_Cons_Unidade = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    StatusBar1: TStatusBar;
    Q_Cons_Unidade: TOracleDataSet;
    Ds_Cons_Unidade: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_UnidadeDESCRICAO: TStringField;
    Q_Cons_UnidadeDESCRICAO_RIEX: TStringField;
    Q_Cons_UnidadeID_UNIDADE: TStringField;
    cxGrid_ConsultaDBTableView1ID_UNIDADE: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO_RIEX: TcxGridDBColumn;
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Unidade: TFrm_Cons_Unidade;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Unidade.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Unidade.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Unidade.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Unidade do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_descricao',Edt_Consulta.Text+'%');
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Unidade.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1ID_UNIDADE.Focused;
  end;
end;

procedure TFrm_Cons_Unidade.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(P_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(P_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Unidade.Session := P_Sessao;
  Q_Cons_Unidade.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

end.
