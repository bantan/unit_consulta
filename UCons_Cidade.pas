unit UCons_Cidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  TFrm_Cons_Cidade = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    StatusBar1: TStatusBar;
    Q_Cons_Cidade: TOracleDataSet;
    Ds_Cons_Cidade: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_CidadeROWNUM: TFloatField;
    Q_Cons_CidadeCIDADE: TStringField;
    Q_Cons_CidadeRODO_FLUVIAL: TStringField;
    Q_Cons_CidadeCODIGO_IBGE: TIntegerField;
    cxGrid_ConsultaDBTableView1ROWNUM: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1CODIGO_CIDADE: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1CIDADE: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1UF: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1REGIAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1COD_ZONAFRANCA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1RODO_FLUVIAL: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1CODIGO_IBGE: TcxGridDBColumn;
    Q_Cons_CidadeID_CIDADE: TFloatField;
    Q_Cons_CidadeID_UNIDADE_FEDERACAO: TStringField;
    Q_Cons_CidadeID_REGIAO: TFloatField;
    Q_Cons_CidadeID_SUB_REGIAO: TFloatField;
    Q_Cons_CidadeID_PAIS: TFloatField;
    Q_Cons_CidadeNOME_PAIS: TStringField;
    Q_Cons_CidadeCODIGO_ZONAFRANCA: TStringField;
    Q_Cons_CidadeREGIAO: TStringField;
    Q_Cons_CidadeSUB_REGIAO: TStringField;
    cxGrid_ConsultaDBTableView1SUB_REGIAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1NOME_PAIS: TcxGridDBColumn;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Cidade: TFrm_Cons_Cidade;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Cidade.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_CIDADE.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Cidade.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Cidade.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Cidade.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Cidade do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_cidade',Edt_Consulta.Text+'%');
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Cidade.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1CIDADE.Focused;
  end;
end;

procedure TFrm_Cons_Cidade.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Cidade.Session := P_Sessao;
  Q_Cons_Cidade.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

end.
