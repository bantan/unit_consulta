unit UCons_Referencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi, DB, ComCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, Bde.DBTables, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, RxMemDS, RxLookup;

type
  TFrm_Cons_Referencia = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    StatusBar1: TStatusBar;
    Q_Cons_Referencia: TOracleDataSet;
    Ds_Cons_Referencia: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Label1: TLabel;
    DsMem: TDataSource;
    Label3: TLabel;
    Q_Cons_ReferenciaID_REFERENCIA: TStringField;
    Q_Cons_ReferenciaDESCRICAO: TStringField;
    Q_Cons_ReferenciaDESCRICAO_ETIQUETA: TStringField;
    Q_Cons_ReferenciaID_FORMA: TStringField;
    Q_Cons_ReferenciaID_COR: TStringField;
    Grid_ConsultaDBTableView1: TcxGridDBTableView;
    Grid_ConsultaLevel1: TcxGridLevel;
    Grid_Consulta: TcxGrid;
    Grid_ConsultaDBTableView1ID_REFERENCIA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1DESCRICAO_ETIQUETA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1ID_FORMA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1ID_COR: TcxGridDBColumn;
    RxDBLookupCombo1: TRxDBLookupCombo;
    RxMemoryData1: TRxMemoryData;
    RxMemoryData1COLUNA: TStringField;
    RxMemoryData1DESCRICAO: TStringField;

    procedure Edt_ConsultaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBLookupComboChange(Sender: TObject);
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Grid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure Grid_ConsultaDBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    p_campo_default :String;
    { Public declarations }
  end;

var
  Frm_Cons_Referencia: TFrm_Cons_Referencia;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Referencia.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_REFERENCIA.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Referencia.Edt_ConsultaChange(Sender: TObject);
begin
  Q_Cons_Referencia.Close;
  if Edt_Consulta.Text <> EmptyStr then
  begin
    Q_Cons_Referencia.SetVariable('DESCRICAO','%'+Edt_Consulta.Text+'%');
    Q_Cons_Referencia.Open;
    if Q_Cons_Referencia.RecordCount = 0 then
      StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
    else if Q_Cons_Referencia.RecordCount = 1 then
      StatusBar1.Panels[0].Text := '1 registro encontrado!'
    else
      StatusBar1.Panels[0].Text := inttostr(Q_Cons_Referencia.RecordCount)+' registros encontrados!'
  end;
end;

procedure TFrm_Cons_Referencia.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    Grid_Consulta.SetFocus;
    Grid_ConsultaDBTableView1ID_REFERENCIA.Focused := TRUE;
  end;
end;

procedure TFrm_Cons_Referencia.Edt_ConsultaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (ssCtrl in Shift) and (chr(Key) in ['R', 'r']) then
    begin
        if RxMemoryData1.Locate('DESCRICAO','Refer�ncia',[]) then
          RxDBLookupCombo1.DisplayValue := RxMemoryData1DESCRICAO.AsString;
    end
    else if (ssCtrl in Shift) and (chr(Key) in ['D', 'd']) then
    begin
        if RxMemoryData1.Locate('Descricao','Descri��o',[]) then
          RxDBLookupCombo1.DisplayValue := RxMemoryData1DESCRICAO.AsString;
    end;
end;

procedure TFrm_Cons_Referencia.FormShow(Sender: TObject);
var i: integer;
begin
    Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
    Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
    Q_Cons_Referencia.Session := P_Sessao;
    Q_Cons_Referencia.Session.Connected := True;

    Edt_Consulta.Clear;
    Edt_Consulta.SetFocus;

    RxMemoryData1.close;
    RxMemoryData1.Open;

    for i := 0 to Q_Cons_Referencia.FieldCount -1 do
    begin
        RxMemoryData1.Append;
        RxMemoryData1COLUNA.Value    := Q_Cons_Referencia.Fields[i].FieldName;
        RxMemoryData1DESCRICAO.Value := Q_Cons_Referencia.Fields[i].DisplayLabel;
    end;

    RxMemoryData1.Locate('COLUNA',P_Campo_Default,[]);
    RxDBLookupCombo1.DisplayValue := RxMemoryData1DESCRICAO.AsString;

  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

procedure TFrm_Cons_Referencia.JvDBLookupComboChange(Sender: TObject);
begin
    Lbl_Consulta.Caption := RxDBLookupCombo1.Text;
end;

procedure TFrm_Cons_Referencia.Grid_ConsultaDBTableView1DblClick(
  Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Referencia.Grid_ConsultaDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

end.
