unit UCons_Estoque_Tipo_Movimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator,
  cxCheckBox;

type
  TFrm_Cons_Estoque_Tipo_Movimento = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    StatusBar1: TStatusBar;
    Q_Cons_Estoque_Tipo_Movimento: TOracleDataSet;
    Ds_Cons_Estoque_Tipo_Movimento: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    cxGrid_Consulta: TcxGrid;
    Q_Cons_Estoque_Tipo_MovimentoID_ESTOQUE_TIPO_MOVIMENTO: TFloatField;
    Q_Cons_Estoque_Tipo_MovimentoDESCRICAO: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoENTRADA_SAIDA: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoENTRADA_COMPRA: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoSAIDA_CONSUMO: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoSAIDA_VENDA: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoTRANSFERENCIA: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoCALCULA_CUSTO_MEDIO: TStringField;
    Q_Cons_Estoque_Tipo_MovimentoID_MOVIMENTO_CONTRA_PARTIDA: TFloatField;
    Q_Cons_Estoque_Tipo_MovimentoDESCRICAO_CONTRA_PARTIDA: TStringField;
    cxGrid_ConsultaDBTableView1ID_ESTOQUE_TIPO_MOVIMENTO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ENTRADA_SAIDA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ENTRADA_COMPRA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1SAIDA_CONSUMO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1SAIDA_VENDA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1TRANSFERENCIA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1CALCULA_CUSTO_MEDIO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ID_MOVIMENTO_CONTRA_PARTIDA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO_CONTRA_PARTIDA: TcxGridDBColumn;
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;

    P_ENTRADA_SAIDA        : String;
    P_ENTRADA_COMPRA       : String;
    P_SAIDA_CONSUMO        : String;
    P_SAIDA_VENDA          : String;
    P_TRANSFERENCIA        : String;
    P_CALCULA_CUSTO_MEDIO  : String;


    { Public declarations }
  end;

var
  Frm_Cons_Estoque_Tipo_Movimento: TFrm_Cons_Estoque_Tipo_Movimento;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Estoque_Tipo_Movimento.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Estoque_Tipo_Movimento.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Estoque_Tipo_Movimento.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Estoque_Tipo_Movimento do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_ENTRADA_SAIDA',P_ENTRADA_SAIDA)
      else
        SetVariable('P_ENTRADA_SAIDA',null);

      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_ENTRADA_COMPRA', P_ENTRADA_COMPRA)
      else
        SetVariable('P_ENTRADA_COMPRA', null);

      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_SAIDA_CONSUMO', P_SAIDA_CONSUMO)
      else
        SetVariable('P_SAIDA_CONSUMO', null);

      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_SAIDA_VENDA', P_SAIDA_VENDA)
      else
        SetVariable('P_SAIDA_VENDA', null);

      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_TRANSFERENCIA', P_TRANSFERENCIA)
      else
        SetVariable('P_TRANSFERENCIA', null);

      if P_ENTRADA_SAIDA <> EmptyStr then
        SetVariable('P_CALCULA_CUSTO_MEDIO', P_CALCULA_CUSTO_MEDIO)
      else

      SetVariable('p_descricao','%'+Edt_Consulta.Text+'%');
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Estoque_Tipo_Movimento.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1ID_ESTOQUE_TIPO_MOVIMENTO.Focused;
  end;
end;

procedure TFrm_Cons_Estoque_Tipo_Movimento.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Estoque_Tipo_Movimento.Session := P_Sessao;
  Q_Cons_Estoque_Tipo_Movimento.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

end.
