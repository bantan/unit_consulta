unit UCons_Condicao_Pagamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  DB, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter;

type
  TFrm_Cons_Condicao_Pagamento = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    StatusBar1: TStatusBar;
    Q_Cons_Condicao_Pagamento: TOracleDataSet;
    Ds_Cons_Condicao_Pagamento: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_Condicao_Pagamento_Desd: TOracleDataSet;
    Ds_Cons_Condicao_Pagamento_Desd: TDataSource;
    Q_Cons_Condicao_Pagamento_DesdID_CONDICAO_PAGAMENTO: TFloatField;
    Q_Cons_Condicao_Pagamento_DesdDIAS: TIntegerField;
    Q_Cons_Condicao_PagamentoID_CONDICAO_PAGAMENTO: TFloatField;
    Q_Cons_Condicao_PagamentoDESCRICAO: TStringField;
    Q_Cons_Condicao_PagamentoTIPO: TStringField;
    Q_Cons_Condicao_PagamentoESPECIAL: TStringField;
    Panel1: TPanel;
    cxGrid_Consulta: TcxGrid;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_ConsultaDBTableView1ID_CONDICAO_PAGAMENTO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1TIPO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ESPECIAL: TcxGridDBColumn;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Q_Cons_Condicao_PagamentoAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Condicao_Pagamento: TFrm_Cons_Condicao_Pagamento;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Condicao_Pagamento.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_CONDICAO_PAGAMENTO.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Condicao_Pagamento.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Condicao_Pagamento.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Condicao_Pagamento.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Condicao_Pagamento do
  begin
    Close;
    SetVariable('p_descricao',Edt_Consulta.Text+'%');
    Open;
    if RecordCount = 0 then
      StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
    else if RecordCount = 1 then
      StatusBar1.Panels[0].Text := '1 registro encontrado!'
    else
      StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
  end;
end;

procedure TFrm_Cons_Condicao_Pagamento.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1ID_CONDICAO_PAGAMENTO.Focused := TRUE;
  end;
end;

procedure TFrm_Cons_Condicao_Pagamento.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Condicao_Pagamento.Session := P_Sessao;
  Q_Cons_Condicao_Pagamento.Session.Connected := True;
  Q_Cons_Condicao_Pagamento_Desd.Session := P_Sessao;
  Q_Cons_Condicao_Pagamento_Desd.Session.Connected := True;
  Q_Cons_Condicao_Pagamento_Desd.close;
  Q_Cons_Condicao_Pagamento_Desd.open;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

procedure TFrm_Cons_Condicao_Pagamento.Q_Cons_Condicao_PagamentoAfterOpen(
  DataSet: TDataSet);
begin
  Q_Cons_Condicao_Pagamento_Desd.Close;
  Q_Cons_Condicao_Pagamento_Desd.SetVariable('id_condicao_pagamento',Q_Cons_Condicao_PagamentoID_CONDICAO_PAGAMENTO.Value);
  Q_Cons_Condicao_Pagamento_Desd.Open;
end;

end.
