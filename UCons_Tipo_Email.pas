unit UCons_Tipo_Email;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  TFrm_Cons_Tipo_Email = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    StatusBar1: TStatusBar;
    Q_Cons_Tipo_Email: TOracleDataSet;
    Q_Opcao_Menu: TOracleDataSet;
    Ds_Cons_Tipo_Email: TDataSource;
    Panel_Titulo: TPanel;
    Q_Opcao_MenuCODIGO_MENU: TIntegerField;
    Q_Opcao_MenuCODIGO_MODULO: TIntegerField;
    Q_Opcao_MenuNIVEL_1: TIntegerField;
    Q_Opcao_MenuNIVEL_2: TIntegerField;
    Q_Opcao_MenuNIVEL_3: TIntegerField;
    Q_Opcao_MenuNIVEL_4: TIntegerField;
    Q_Opcao_MenuNOME_SISTEMA: TStringField;
    Q_Opcao_MenuEXECUTAVEL: TStringField;
    Q_Opcao_MenuTIPO: TStringField;
    Q_Opcao_MenuDESCRICAO: TStringField;
    Q_Opcao_MenuBANCO: TStringField;
    Q_Opcao_MenuACESSO: TIntegerField;
    Q_Opcao_MenuINCLUSAO: TIntegerField;
    Q_Opcao_MenuALTERACAO: TIntegerField;
    Q_Opcao_MenuEXCLUSAO: TIntegerField;
    Q_Opcao_MenuRELATORIO: TIntegerField;
    Q_Opcao_MenuEMAIL: TIntegerField;
    Q_Opcao_MenuPDF: TIntegerField;
    Q_Opcao_MenuTXT: TIntegerField;
    Q_Opcao_MenuWORD: TIntegerField;
    Q_Opcao_MenuHTML: TIntegerField;
    Q_Opcao_MenuEXCEL: TIntegerField;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_Tipo_EmailTIPO_EMAIL: TIntegerField;
    Q_Cons_Tipo_EmailDESCRICAO: TStringField;
    cxGrid_ConsultaDBTableView1TIPO_EMAIL: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    P_Sessao  : TOracleSession;
    P_user, p_password : String;
    parametro_novo  :String;
    m_diretorio_sistema : String;
    { Public declarations }
  end;

var
  Frm_Cons_Tipo_Email: TFrm_Cons_Tipo_Email;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Tipo_Email.btn_CadastrarClick(Sender: TObject);
begin
  Q_Opcao_Menu.Session := P_Sessao;
  Q_Opcao_Menu.Session.Connected := True;

  Q_Opcao_Menu.close;
  Q_Opcao_Menu.open;
  if Q_Opcao_MenuACESSO.Value <> 1 then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end;

  parametro_novo := '5'+p_user+
                    '$#'+p_password+
                    '$*'+Q_Opcao_MenuBANCO.AsString+
                    '$@'+Q_Opcao_MenuEXECUTAVEL.AsString+
                    '$%'+Troca_Espaco_por_Grau(Q_Opcao_MenuNOME_SISTEMA.AsString)+
                    '$a'+Q_Opcao_MenuACESSO.AsString+
                    '$b'+Q_Opcao_MenuINCLUSAO.AsString+
                    '$c'+Q_Opcao_MenuALTERACAO.AsString+
                    '$d'+Q_Opcao_MenuEXCLUSAO.AsString+
                    '$e'+Q_Opcao_MenuRELATORIO.AsString+
                    '$f'+Q_Opcao_MenuEMAIL.AsString+
                    '$g'+Q_Opcao_MenuPDF.AsString+
                    '$h'+Q_Opcao_MenuTXT.AsString+
                    '$i'+Q_Opcao_MenuWORD.AsString+
                    '$j'+Q_Opcao_MenuHTML.AsString+
                    '$k'+Q_Opcao_MenuEXCEL.AsString;
  ShellExecute(0,'open', pchar(Q_Opcao_MenuEXECUTAVEL.AsString), PChar(parametro_novo), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Tipo_Email.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Tipo_Email.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Tipo_Email.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Tipo_Email do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_descricao',Edt_Consulta.Text+'%');
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Tipo_Email.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1TIPO_EMAIL.Focused;
  end;
end;

procedure TFrm_Cons_Tipo_Email.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(m_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(m_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Tipo_Email.Session := P_Sessao;
  Q_Cons_Tipo_Email.Session.Connected := True;
end;

end.
