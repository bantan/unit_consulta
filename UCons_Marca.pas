unit UCons_Marca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid;

type
  TFrm_Cons_Marca = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    StatusBar1: TStatusBar;
    Q_Cons_Marca: TOracleDataSet;
    Ds_Cons_Marca: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_MarcaDESCRICAO: TStringField;
    Q_Cons_MarcaATIVO: TStringField;
    Q_Cons_MarcaID_MARCA: TFloatField;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_Consulta: TcxGrid;
    cxGrid_ConsultaDBTableView1DESCRICAO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ATIVO: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1ID_MARCA: TcxGridDBColumn;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Marca: TFrm_Cons_Marca;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Marca.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_MARCA.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;


procedure TFrm_Cons_Marca.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Marca do
  begin
    Close;
    if Edt_Consulta.Text <> EmptyStr then
    begin
      SetVariable('p_marca',Edt_Consulta.Text+'%');
      Open;
      if RecordCount = 0 then
        StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
      else if RecordCount = 1 then
        StatusBar1.Panels[0].Text := '1 registro encontrado!'
      else
        StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
    end;
  end;
end;

procedure TFrm_Cons_Marca.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    cxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1DESCRICAO.Focused;
  end;
end;

procedure TFrm_Cons_Marca.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Marca.Session := P_Sessao;
  Q_Cons_Marca.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

end.
