unit UCons_Empresa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,DB, ComCtrls, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  Grids, DBGrids, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLookAndFeels, cxLookAndFeelPainters, cxNavigator,
  Bde.DBTables, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, RxMemDS, RxLookup;

type
  TFrm_Cons_Empresa = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    StatusBar1: TStatusBar;
    Q_Cons_Empresa: TOracleDataSet;
    Ds_Cons_Empresa: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_EmpresaFISICA_JURIDICA: TStringField;
    Q_Cons_EmpresaRAZAO_SOCIAL: TStringField;
    Q_Cons_EmpresaFANTASIA: TStringField;
    Q_Cons_EmpresaINSCRICAO_ESTADUAL: TStringField;
    Q_Cons_EmpresaINSCRICAO_MUNICIPAL: TStringField;
    Q_Cons_EmpresaRG: TStringField;
    Q_Cons_EmpresaCEP: TStringField;
    Q_Cons_EmpresaENDERECO: TStringField;
    Q_Cons_EmpresaCOMPLEMENTO_ENDERECO: TStringField;
    Q_Cons_EmpresaNUMERO: TIntegerField;
    Q_Cons_EmpresaBAIRRO: TStringField;
    Q_Cons_EmpresaCIDADE: TStringField;
    Q_Cons_EmpresaUF: TStringField;
    Q_Cons_EmpresaCODIGO_IBGE: TIntegerField;
    Q_Cons_EmpresaPAIS: TStringField;
    Q_Cons_EmpresaCODIGO_SUFRAMA: TStringField;
    Q_Cons_EmpresaUNIDADE_NEGOCIO: TStringField;
    Q_Cons_EmpresaPAGAMENTO_CARTEIRA: TStringField;
    Q_Cons_EmpresaDESC_SITUACAO: TStringField;
    Q_Cons_EmpresaNOME_TRADER: TStringField;
    DsMem: TDataSource;
    Label1: TLabel;
    Label3: TLabel;
    Q_Cons_EmpresaTIPO_EMPRESA: TStringField;
    Grid_ConsultaDBTableView1: TcxGridDBTableView;
    Grid_ConsultaLevel1: TcxGridLevel;
    Grid_Consulta: TcxGrid;
    Grid_ConsultaDBTableView1CNPJ_CPF: TcxGridDBColumn;
    Grid_ConsultaDBTableView1FISICA_JURIDICA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1RAZAO_SOCIAL: TcxGridDBColumn;
    Grid_ConsultaDBTableView1FANTASIA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1INSCRICAO_ESTADUAL: TcxGridDBColumn;
    Grid_ConsultaDBTableView1INSCRICAO_MUNICIPAL: TcxGridDBColumn;
    Grid_ConsultaDBTableView1RG: TcxGridDBColumn;
    Grid_ConsultaDBTableView1CEP: TcxGridDBColumn;
    Grid_ConsultaDBTableView1ENDERECO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1COMPLEMENTO_ENDERECO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1NUMERO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1BAIRRO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1CIDADE: TcxGridDBColumn;
    Grid_ConsultaDBTableView1UF: TcxGridDBColumn;
    Grid_ConsultaDBTableView1CODIGO_IBGE: TcxGridDBColumn;
    Grid_ConsultaDBTableView1PAIS: TcxGridDBColumn;
    Grid_ConsultaDBTableView1CODIGO_SUFRAMA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1UNIDADE_NEGOCIO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1PAGAMENTO_CARTEIRA: TcxGridDBColumn;
    Grid_ConsultaDBTableView1DESC_SITUACAO: TcxGridDBColumn;
    Grid_ConsultaDBTableView1NOME_TRADER: TcxGridDBColumn;
    Grid_ConsultaDBTableView1TIPO_EMPRESA: TcxGridDBColumn;
    Q_Cons_EmpresaID_EMPRESA: TFloatField;
    Q_Cons_EmpresaCNPJ_CPF: TStringField;
    Grid_ConsultaDBTableView1ID_EMPRESA: TcxGridDBColumn;
    RxMem: TRxMemoryData;
    RxDBLookupCombo: TRxDBLookupCombo;
    RxMemCOLUNA: TStringField;
    RxMemDESCRICAO: TStringField;
    procedure Edt_ConsultaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RxDBLookupComboChange(Sender: TObject);
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Grid_ConsultaDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure Grid_ConsultaDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    P_Campo_Default, P_Tipo_Empresa: string;
    P_UnidadeNegocios : string;
    { Public declarations }
  end;

var
  Frm_Cons_Empresa: TFrm_Cons_Empresa;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Empresa.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_EMPRESA.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;

procedure TFrm_Cons_Empresa.cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Empresa.cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Empresa.Edt_ConsultaChange(Sender: TObject);
begin
  Q_Cons_Empresa.Close;

  if Edt_Consulta.Text <> EmptyStr then
  begin

    // Raz�o Social ou campo selecionado
    if RxDBLookupCombo.Text <> EmptyStr then
      Q_Cons_Empresa.sql.Strings[17] := '   and  ' + RxMemCOLUNA.AsString + ' like ' + QuotedStr(Edt_Consulta.Text + '%')
    else
      Q_Cons_Empresa.sql.Strings[17] := '   and  e.razao_social like ' + QuotedStr(Edt_Consulta.Text + '%');

    // Unidade de Neg�cio
    if P_UnidadeNegocios = 'S' then
      Q_Cons_Empresa.sql.Strings[18] := '   and e.unidade_negocio = ''S'' '
    else
      Q_Cons_Empresa.sql.Strings[18] := ' ';

    // Tipo de Empresa
    if P_Tipo_Empresa <> EmptyStr then
    begin
      Q_Cons_Empresa.sql.Strings[19] := '   and (e.id_empresa) in (select et.id_empresa from pm.empresa_tipo_empresa et  ';
      Q_Cons_Empresa.sql.Strings[20] := '                         where et.id_empresa = e.id_empresa   ';
      Q_Cons_Empresa.sql.Strings[21] := '                           and et.id_tipo_empresa in (' + P_Tipo_Empresa + '))';
    end
    else
    begin
      Q_Cons_Empresa.sql.Strings[19] := ' ';
      Q_Cons_Empresa.sql.Strings[20] := ' ';
      Q_Cons_Empresa.sql.Strings[21] := ' ';
    end;

    Q_Cons_Empresa.Sql.SaveToFile('c:\Prime\Sqls\Q_Cons_Empresa.txt');
    Q_Cons_Empresa.Open;
    if Q_Cons_Empresa.RecordCount = 0 then
      StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
    else if Q_Cons_Empresa.RecordCount = 1 then
      StatusBar1.Panels[0].Text := '1 registro encontrado!'
    else
      StatusBar1.Panels[0].Text := inttostr(Q_Cons_Empresa.RecordCount)+' registros encontrados!'

   end;
end;

procedure TFrm_Cons_Empresa.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    Grid_Consulta.SetFocus;
//    Grid_Consulta.FocusedField := Q_Cons_EmpresaCNPJ_CPF;
  end;
end;

procedure TFrm_Cons_Empresa.Edt_ConsultaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (chr(Key) in ['F', 'f']) then
  begin
    if RxMem.Locate('Descricao','Fantasia',[]) then
      RxDBLookupCombo.DisplayValue := RxMemDESCRICAO.AsString;
  end
  else if (ssCtrl in Shift) and (chr(Key) in ['R', 'r']) then
  begin
    if RxMem.Locate('Descricao','Raz�o Social',[]) then
      RxDBLookupCombo.DisplayValue := RxMemDESCRICAO.AsString;
  end;
end;

procedure TFrm_Cons_Empresa.FormShow(Sender: TObject);
var i :  integer;
begin
  if P_UnidadeNegocios = EmptyStr then
    P_UnidadeNegocios := 'N';

  Q_Cons_Empresa.Session := P_Sessao;
  Q_Cons_Empresa.Session.Connected := true;

  Edt_Consulta.Clear;
  Edt_Consulta.SetFocus;

  RxMem.close;
  RxMem.Open;

  for i := 0 to Q_Cons_Empresa.FieldCount -1 do
  begin
    RxMem.Append;
    RxMemCOLUNA.Value := Q_Cons_Empresa.Fields[i].FieldName;
    RxMemDESCRICAO.Value := Q_Cons_Empresa.Fields[i].DisplayLabel;
  end;

  if P_Campo_Default <> EmptyStr then
    RxMem.Locate('COLUNA',P_Campo_Default,[])
  else
    RxMem.Locate('COLUNA','RAZAO_SOCIAL',[]);

//  RxDBLookupCombo.DisplayValue := RxMemDESCRICAO.AsString;
  RxDBLookupCombo.DisplayValue := RxMemDESCRICAO.AsString;
  Lbl_Consulta.Caption := RxDBLookupCombo.Text;

  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

procedure TFrm_Cons_Empresa.RxDBLookupComboChange(Sender: TObject);
begin
  Lbl_Consulta.Caption := RxDBLookupCombo.Text;
end;

procedure TFrm_Cons_Empresa.Grid_ConsultaDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

procedure TFrm_Cons_Empresa.Grid_ConsultaDBTableView1DblClick(
  Sender: TObject);
begin
    ModalResult := mrOk;
end;

end.
