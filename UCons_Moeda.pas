unit UCons_Moeda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, ExtCtrls, OracleData, Oracle, shellapi,
  ComCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, cxLookAndFeels,
  cxLookAndFeelPainters, cxNavigator, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter;

type
  TFrm_Cons_Moeda = class(TForm)
    Panel_Alto: TPanel;
    Panel_Baixo: TPanel;
    btn_Ok: TBitBtn;
    btn_Cancelar: TBitBtn;
    btn_Cadastrar: TBitBtn;
    Lbl_Consulta: TLabel;
    Edt_Consulta: TEdit;
    StatusBar1: TStatusBar;
    Q_Cons_Moeda: TOracleDataSet;
    Ds_Cons_Moeda: TDataSource;
    Panel_Titulo: TPanel;
    Image_Empresa: TImage;
    Image_Prime: TImage;
    Q_Cons_MoedaID_MOEDA: TStringField;
    Q_Cons_MoedaSIGLA: TStringField;
    cxGrid_Consulta: TcxGrid;
    cxGrid_ConsultaDBTableView1: TcxGridDBTableView;
    cxGrid_ConsultaLevel1: TcxGridLevel;
    cxGrid_ConsultaDBTableView1ID_MOEDA: TcxGridDBColumn;
    cxGrid_ConsultaDBTableView1SIGLA: TcxGridDBColumn;
    procedure btn_CadastrarClick(Sender: TObject);
    procedure Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure Edt_ConsultaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1DblClick(Sender: TObject);
    procedure cxGrid_ConsultaDBTableView1KeyPress(Sender: TObject;
      var Key: Char);
  private
    { Private declarations }
  public
    p_sessao : TOracleSession;
    p_diretorio_sistema : String;
    p_descricao : String;
    { Public declarations }
  end;

var
  Frm_Cons_Moeda: TFrm_Cons_Moeda;

implementation

uses Unit_funcoes;

{$R *.dfm}

procedure TFrm_Cons_Moeda.btn_CadastrarClick(Sender: TObject);
var m_texto, m_exec :string;
begin
  m_texto := checa_permissao_cadastro('CAD_MOEDA.EXE',P_Sessao);
  m_exec := UpperCase(copy(m_texto,pos('$@',m_texto)+2,pos('$%',m_texto)-(pos('$@',m_texto)+2)));
  if m_texto = '0' then
  begin
    MessageDlg('Voc� n�o tem permiss�o de acesso para este sistema!',mtInformation,[mbOk],0);
    Abort;
  end
  else
    ShellExecute(0,'open', pchar(m_exec), PChar(m_texto), ' ',SW_SHOW);
end;


procedure TFrm_Cons_Moeda.Edt_ConsultaChange(Sender: TObject);
begin
  with Q_Cons_Moeda do
  begin
    Close;
    SetVariable('id_moeda',Edt_Consulta.Text+'%');
    Open;
    if RecordCount = 0 then
      StatusBar1.Panels[0].Text := 'Nenhum registro encontrado!'
    else if RecordCount = 1 then
      StatusBar1.Panels[0].Text := '1 registro encontrado!'
    else
      StatusBar1.Panels[0].Text := inttostr(RecordCount)+' registros encontrados!'
  end;
end;

procedure TFrm_Cons_Moeda.Edt_ConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    CxGrid_Consulta.SetFocus;
    cxGrid_ConsultaDBTableView1ID_MOEDA.Focused := True;
  end;
end;

procedure TFrm_Cons_Moeda.FormShow(Sender: TObject);
begin
  Image_Empresa.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_empresa.ico');
  Image_Prime.Picture.LoadFromFile(p_diretorio_sistema+'Imagens\logo_prime.ico');
  Q_Cons_Moeda.Session := P_Sessao;
  Q_Cons_Moeda.Session.Connected := True;
  if p_descricao <> EmptyStr then
    Edt_Consulta.Text := p_descricao;
end;

procedure TFrm_Cons_Moeda.cxGrid_ConsultaDBTableView1DblClick(
  Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrm_Cons_Moeda.cxGrid_ConsultaDBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
  if key = #13 then
    ModalResult := mrOk;
end;

end.
